---
title: Essay Utopie
---

**De digitale utopie**

De manieren waarop adverteerbedrijven data verzamelen zijn de afgelopen jaren veel meer invasief geworden, zoekgeschiedenis, gebruiksgegevens van tenminste 10 miljoen websites, locatie; vrijwel alles wat ze kunnen verzamelen wordt opgeslagen om een profiel voor persoonlijk gerichte advertenties op te bouwen.  

Dit is een (controversieel) voorbeeld van technologische vooruitgang, net als vele andere in de laatste jaren. Wat zou er gebeuren als dit met hetzelfde tempo doorgaat, of zelfs steeds sneller wordt? 

Het eerste wat moet gebeuren als de mensheid technologie verder vooruit wil krijgen is om te stoppen met de focus op individuele prestaties. Het gebeurt te vaak dat technologie een race is tussen bepaalde groepen, en alleen de groep die het beste of eerste is de roem (of geld) krijgt. Een voorbeeld is de 'Space Race', de VS en de Sovjetunie hadden vele malen meer kunnen bereiken als ze samen hadden gewerkt aan de ruimtevaart. 

Verder moet technologie transparant blijven, mensen moeten ervoor kunnen kiezen om geen persoonlijke data opgeslagen te houden bij de services waar zij gebruik van maken. Denk aan gebruiksstatistieken, zoekgeschiedenis, locatie, alles wat niet absoluut nodig is om de service te gebruiken. En logischerwijs moeten mensen dus ook bewust worden gemaakt van de data die een service wil verzamelen. De GDPR heeft hier al redelijke vooruitgang mee geboekt maar helaas zijn er vaak sluwe manieren waarop de bedrijven deze wetten omzeilen. 

Zodra het paradigma rond samenwerking en transparante technologie op die manier veranderd kan er fantastische vooruitgang plaatsvinden. Het eerste wat voor deze nieuwe wereld zal veranderen is dat mensen uitgerust worden met ‘mentale computers’ die samenwerken met de hersenen van de drager om verschillende taken vele malen efficiënter te maken. 

Een voorbeeld van een functie die zo een computer kan hebben is perfect geheugen. In plaats van zoeken in je agenda naar wat er vandaag op de planning staat heb je dit precies in je hoofd zitten, samen met een perfecte klok. Verder zou het dan ook mogelijk zijn om precieze berekeningen te doen voor hoe lang een bepaalde taak zal duren. Mensen hoeven dus nooit meer te laat te komen.  

Verder zou een mentale computer ongelukken in het verkeer door een menselijke fout kunnen voorkomen. Het inschattingsvermogen van mensen is niet meer een vaardigheid, maar een zekerheid. De computer in de auto en de mentale computer kunnen samenwerken om de mentale computer de nodige informatie van verschillende sensoren te geven, zodat de bestuurder erg precies kan berekenen hoe hard die moet sturen of remmen. 

Een probleem met de manier waarop mensen in de huidige wereld informatie uitwisselen is dat er vrijwel altijd een manier is waarop het afgeluisterd kan worden. Communicatie tussen mentale computers zal de kans hierop nihil maken door het gebruik van versleutelde communicatiekanalen. De mensen die de mentale computers hebben zullen dan zelf kunnen bepalen hoe privé hun gesprekken zijn omdat personen waarvoor het bericht niet bestemd is er niks van kunnen maken. 

De code van het besturingssysteem van deze computer zal publiek beschikbaar zijn, zodat iedereen kan verifiëren dat er niks achter de schermen gebeurt wat schadelijk is. En als je wel iets schadelijks vindt, kun je dat rapporteren voor een geldbeloning. Het is hiervoor noodzakelijk dat de computer niet de emoties van de drager kan manipuleren, en dat de drager alleen de computer gebruikt als ze dat wil.  

De digitale utopie zou erg goed zijn voor een efficiëntere samenleving, maar er zijn een hoop risico’s aan de vereiste technologie die eerst zo goed als uitgesloten moeten worden. Het grootste risico zou zijn dat de mentale computers op een manier gehackt worden. De geldbeloningen voor fouten in de code zou dit risico zo goed als uitsluiten, omdat er dan een drijfveer is om het platform veilig te houden. 


**Bronnen:**

[https://marketingland.com/google-analytics-is-installed-on-more-than-10-million-websites-9935](https://marketingland.com/google-analytics-is-installed-on-more-than-10-million-websites-9935)

[https://www.google.com/intl/en/policies/privacy/google_privacy_policy_en.pdf](https://www.google.com/intl/en/policies/privacy/google_privacy_policy_en.pdf)
