---
title: Fin de Siècle
---

**Samenvatting start van beweging van 80:**

De beweging van tachtig ontstond door de sterk conservatieve tijd in het midden van de 19e eeuw. In het midden van de 19e eeuw werden alle pogingen tot vernieuwing ook altijd moreel getoetst, hierdoor was vernieuwing op zichzelf al een taboe. 

De tachtigers waren jong en hadden veel vernieuwende ideeën, veel van hen waren studenten of kunstenaars en de meerderheid kwam uit Amsterdam. Ook hun woonplaats had invloed op de vernieuwende ideeën die ze besproken over bijvoorbeeld de literatuur. In de laatste decennia van de 19 eeuw vernieuwde er veel in Amsterdam zoals in de bouw, de industrie, de wetenschap en natuurlijk in de kunst. Al deze vernieuwingen zorgde voor een mogelijkheid voor de Tachtigers om vernieuwende idealen, vormen van kunst en nieuwe manieren van schrijven door te kunnen voeren.  
Bij vernieuwingen hoorde ook het bekritiseren van de oude idealen en een afkeer van de oude wereld. Er kwamen in de kunst en literatuur bekritiserende werken tegenover de toen, conservatieve maatschappij. Helaas is de strikt conservatieve maatschappij ook een oorzaak geweest voor de Tachtigers om hun idealen te koppelen aan het Socialisme. Hierdoor werden hun idealen gekoppeld aan een politiek perspectief in plaats van idealistische ideeën voor een betere wereld en maatschappij. 

In de kunst en literatuur waren de vernieuwingen duidelijk te zien. Er werden veel taboes verbroken, zoals bijvoorbeeld de seksuele onderwerpen, die voorheen onbespreekbaar waren. Ook werd er veel gekeken naar hoe de wereld eerlijker zou kunnen worden voor de armen tegenover de rijken. De verschillen waren destijds namelijk substantieel en werden door de afname in taboes veel beschreven en getekend in de kunst en literatuur. 

 
**Principes van de tachtigers**

Ons onderwerp: _Mei van Herman Gorter met het Impressionisme_

Door het vernieuwende taalgebruik en de beschrijving van de natuur in het gedicht van Herman Gorter, is het goed te zien dat het samen gaat met het Impressionisme. Het Impressionisme was namelijk een vernieuwende stroming waarbij vooral werd gekeken naar de beleving in het moment. Het gedicht ‘Mei’ was zelfs binnen het Impressionisme een vernieuwend gedicht, omdat het in plaats van het dagelijks leven, en de gewone taferelen die daarbij hoorde, ook wonderlijke wezens zoals elven bevatte. Dit was in deze tijd nog niet veel gedaan, ook niet in impressionistische gedichten. 

Een paar duidelijke overeenkomsten met het gedicht ‘Mei’ en het Impressionisme zijn de vernieuwende en vrije gedachtes. In het gedicht Mei, verwerkt Herman Gorter fantasierijke ideeën die vóór het Impressionisme niet bedacht zouden zijn. Er wordt met het Impressionisme afstand gedaan van normen en waarden die volgens de impressionisten te strikt en te beknellend waren. Kunst in het algemeen moest in hun ideologie vrijer worden en hoefde zeker niet altijd/tot bijna nooit een boodschap te bevatten.  

Het gedicht Mei is in het begin van de beweging van 80 uitgebracht. In deze tijd heerste er vooral een strikt kapitalistische ideologie. Mensen die in deze tijd andere of nieuwe ideeën hadden werden genegeerd of negatief beoordeeld.  

Om de streng kapitalistische maatschappij tegen te werken, met als doel een vrijere en eerlijke gemeenschap, gebruikte de Tachtigers (waaronder Herman Gorter) het Impressionisme en verwerkte hun idealistische gedachtes in hun werken. Doordat de Tachtigers vrij extreem tegen hun omgevende kapitalistische maatschappij waren, ontstonden er onder de Tachtigers vele Socialistische ideeën die onbewust toch het doel van de werken politiek maakten. Eén van de doelen die de Tachtigers niet wilde hebben met hun werken. 

Herman Gorter was een Nederlandse dichter/schrijver in de 19 eeuw en hij staat vooral bekend om zijn werk Mei (1889), waarschijnlijk vooral door de lengte en de kwaliteit van het gedicht. Ook al kwam Gorter uit een familie met veel doopsgezinde predikanten, ging hij compleet zijn eigen weg door de meer progressieve en vernieuwende kant te kiezen van de literatuur. Hij had deze optie vooral door de invloeden van Multatuli, Willem Kloos, en niet te vergeten de beweging van Tachtig. Later nam hij alleen afstand van de idealen van de Tachtigers en creëerde hij vooral socialistische werken. 

Het leven van Gorter als kind was niet makkelijk. Ze hadden het niet breed thuis en zijn moeder was dominant. Hierdoor bracht hij veel tijd door bij zijn grootouders in het plaatsje Balk. Hij schreef hier dan ook een aantal van zijn grootste gedichten zoals ‘'Mei’'. Hij ging klassieke talen studeren aan de Gemeente Universiteit van Amsterdam. 

Hij heeft een aantal relaties gehad. Wat opmerkelijk was, is dat hij tijdens deze relaties veel liefdesgedichten schreef. 

In Herman Gorters werk wordt met behulp van metaforen indirect gepraat over de realiteit en er wordt in detail geschreven over de omgeving en het milieu waarin het gedicht zich afspeelt. Hierbij wordt ook in zintuigelijke wijze geschreven voor een makkelijkere inbeelding en beleving voor de lezer. Het doel van Gorter was hierbij vooral om de werkelijke wereld te openbaren aan de lezers, in plaats van de sprookjesachtige wereld waar alleen de mooie dingen van het leven worden beschreven. 

Jonge en naar-vernieuwing-snakkende recensenten waren enthousiast terwijl oudere en meer conservatieve critici moeite hadden met de overdaad aan beelden. Vaak vonden zij het gedicht ook verward. Omdat dit gedicht is geschreven in een nieuwe stijl voor die tijd.  

Ik heb een aantal punten opgesomd die in zijn gedicht duidelijk naar voren kwamen, en dus eventueel ook met het doel van het schrijven van deze gedichten te maken hebben: 
-Liefdesmotief. De liefde van Mei voor Balder, de natuur en schoonheid en de liefde van Balder voor muziek. 
-Natuur. Gorter wilde iets schrijven dat mooi was, vol aandacht en liefde voor de natuur. Gorter vond liefde voor de natuur mooi, daarom beschreef hij het ook uitgebreid. 
Aan het begin van het eerste deel treden de 12 maanden op, op het einde de 12 uren. In het derde deel komen die uren ook weer een aantal keer naar voren. Dat heeft een dubbele functie. Op de eerste plaats geeft ze het gedicht een zekere afronding, de maand vol maken. Op de tweede plaats benadrukken ze een belangrijk aspect van het verhaal: de tijd, ofwel de vergankelijkheid. 
-Muziek. Muziek is heel belangrijk in dit gedicht, vooral bij Balder. 

-Mei ontmoet vier vrouwen in het gedicht. De stroomnimf, de moeder van de stroomnimf, de wolkenspinster Frigga en Idoena en de vrouw van Balder. Drie ervan hebben Balder liefgehad. Idoena overtreft de andere twee: ze is de eeuwige jeugd. Het zgn. ontmoetingsmotief heeft te maken met het liefdesmotief. 
-Herhaaldelijke verwijzingen naar de Griekse mythologie in het eerste deel, en naar de Noorse mythologie in het tweede deel. 

-Verdriet, Mei kan Balder niet meer vinden en is hem dagenlang van haar korte bestaan aan het zoeken.  

Gorter had dus niet een duidelijk doel voor ogen, maar hij schreef over een aantal dingen die hij belangrijk vond, zodat mensen hier indirect wel over gingen nadenken. 

Dat Gorter zoveel aandacht besteedde aan de natuur en omgeving is kenmerkend voor het impressionisme (impressie). Verder speelt het zich af in het alledaagse leven, wat ook kenmerkend is voor het impressionisme. Gorter was actief in dezelfde tijd als de impressionisten, dit was in de tweede helft van de zeventiende eeuw. Samen met deze thema's en algemene kenmerken, gebruikt Gorter ook veel bijvoeglijke naamwoorden om zijn beschrijvingen levendig en accuraat te maken. Het Impressionisme was een vernieuwende kunststroming die zich vooral richtte op lichtinval en werken waar taferelen zichtbaar veel licht bevatte en zich vaak in de buitenlucht afspeelden. Ook komen de doelen van Gorter redelijk overeen met de doelen van impressionisme. 

De werken van Gorter behoren niet volledig tot het impressionisme, maar bevatten impressie en sensatie. Maar omdat gorter erg belangrijk is geweest voor het impressionisme kun je wel zeggen dat hij behoorde tot de impressionisten. 

In conclusie is het Impressionisme volgens ons een hele mooie stroming, die veel invloed heeft gehad op de volgende eeuwen. Het nam in die tijd afstand van de strikte regels binnen de kunst en literatuur en zetten heel andere betekenis en doel neer voor de schrijvers en kunstenaars. Vrijdenken en fantasierijk denken werden ook gestimuleerd binnen deze stroming, en kunst werd een begrip wat geen boodschap hoefde te bevatten om mooi of invloedrijk te zijn. 

In de jaren waarin het Impressionisme ontstond heeft de stroming vooral gezorgd voor een opener beeld tegenover kunst en literatuur. Ook is in het algemeen een realistischer beeld neergezet van de werkelijkheid, voor alle mensen in deze tijd, zowel rijk als arm. 

In onze tijd heeft het Impressionisme volgens ons vooral geleidt tot democratisch denken en gelijkheid onder alle mensen. En de werken van impressionisten zijn nog steeds te zien, deze zijn dus ook erg belangrijk geweest voor de algemene kunst. 
