---
title: Max Havelaar dialoog
---

1“Hoi! Heb je het boek ?” 

2“Max Havelaar bedoel je?” 

1“Ja Max Havelaar.” 

2“Ik vind het een erg mooi boek, ik vraag me alleen af of Multatuli voor het realisme of de romantiek is, ik kan elementen van beide terugzien in het boek. Ik vind dat het boek meer behoort tot het genre Romantiek, omdat er veel aandacht is voor de gevoelens van de schrijver en de hoofdpersonen. Ook wordt er vaak in beschreven hoe de hoofdpersonages denken over bepaalde dingen waar ze mee in contact komen.” 

1“Het lijkt mij duidelijk dat Max Havelaar een typische roman is uit de periode van het Realisme. Max Havelaar zelf is namelijk een idealist en vind, net zoals in de definitie van het Realisme, de idealisatie van de subjectieve beleving zeer belangrijk.” 

2”Dat klopt maar ik zag die eigenschappen van Max Havelaar niet terug in het boek, tevens komt het boek uit de periode van de romantiek, en is het een fictioneel verhaal, wat ook typerend voor romans uit de romantiek is.” 

1”Nou, ik ben nog steeds van mening dat het boek hoort tot het realisme, want volgens mij is het boek geschreven in de periode van de romantiek naar het realisme. Je ziet dat de schrijver een duidelijk doel voor ogen had: De situatie in Nederlands-Indië verbeteren. 

2" Het kan wel zo zijn dat het boek is geschreven in de overgang van de romantiek naar het realisme, maar dat hoeft nog niet te beteken dat het boek bij het realisme hoort. Zo omschrijft de schrijver heel nauwkeurig de situatie en de omgeving waarin het verhaal zich afspeelt, en worden de vriendschapsbanden tussen de personages uitvoerig beschreven. 

1"Maar toch moet het boek realistisch zijn, want kijk nou toch hoe vaak hij de lezers aanspreekt. En ook zie je duidelijke verschillen in sociale lagen. Zo is Batavus droogstoppel een rijke man, en Sjaalman een arme kerel. 

2''Maar in de Max Havelaar zie je verschillende genres terug. Zo is het verhaal van Saidjah en Adinda een soort sprookje, en het verhaal van Max Havelaar meer een soort biografie. Ook wordt het verhaal geschreven uit meerdere perspectieven: Max Havelaar, Multatuli en Batavus Droogstoppel. 

1''Stop! Ik hoor het al we gaan hier niet uit komen. Er zijn zoveel aspecten van beide stromingen dat we niet kunnen zeggen dat het boek tot één stroming behoort, dus zullen we afspreken dat het boek bij beide stromingen hoort? 

2''Is goed. Ik had graag deze discussie gewonnen maar ik zie nu al dat het zinloos is om hierover door te discussiëren. Dus spreken we af dat het boek bij beide genres hoort. 